import React, { useState } from 'react';
import {
  ActivityIndicator,
  KeyboardAvoidingView,
  StatusBar, StyleSheet, Text, View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import {
  ButtonComponent, IconButton, Input, LinkComponent,
} from '../../component';
import { signupUser } from '../../redux';
import { colors, fonts, showError } from '../../utils';

function RegisterScreen({ navigation }) {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const isLoading = useSelector((state) => state.Auth.isLoading);

  const dispatch = useDispatch();

  function isValidPassword(testPassword) {
    const regex = /(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z])/;
    if (testPassword.length === 0) {
      return showError('ERROR : Tolong Masukan Password');
    }
    if (typeof testPassword === 'string') {
      if (regex.test(testPassword) && testPassword.length >= 8) {
        return dispatch(signupUser(name, email, password, navigation));
      }
      return showError('Password minimal ada 1 huruf kecil, huruf besar, dan angka');
    }
    return showError('ERROR : invalid data type');
  }

  function checkEmail(testEmail, testPassword) {
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // pola email
    if (regex.test(testEmail) && typeof testEmail === 'string') {
      return isValidPassword(testPassword);
    } if (testEmail.length === 0) {
      return showError('ERROR : Tolong Isi Email Anda');
    }
    const reg = /(?=.*[@])/;
    if (!reg.test(testEmail)) {
      return showError("ERROR : anda lupa menambahkan '@' pada gmail anda");
    }
    return showError('ERROR : Tolong masukan Gmail anda dengan benar');
  }

  function validation(testName, testEmail, testPassword) {
    if (testName.length === 0) {
      return showError('ERROR : Tolong Masukan fullName');
    }
    if (typeof testName === 'string') {
      return checkEmail(testEmail, testPassword);
    }
    return showError('ERROR : invalid data type fullName');
  }

  const onSubmit = () => {
    validation(name, email, password);
  };

  return (
    <KeyboardAvoidingView behavior="padding" style={styles.page}>
      <StatusBar barStyle="dark-content" backgroundColor={colors.background.primary} />
      <IconButton type="back" onPress={() => navigation.goBack()} />
      <View style={styles.titleWrapper}>
        <Text style={styles.title}>Create Account</Text>
        <Text style={styles.text}>Buat akun untuk akses</Text>
      </View>
      <View style={styles.inputWrapper}>
        <Input placeholder="Full Name" value={name} onChangeText={(text) => setName(text)} testID="input-FullName" />
        <Input placeholder="Email" marginTopInput={10} value={email} onChangeText={(text) => setEmail(text)} testID="input-Email" />
        <Input placeholder="Password" marginTopInput={10} value={password} onChangeText={(text) => setPassword(text)} secureTextEntry testID="input-Password" />
      </View>
      <View style={styles.buttonWrapper}>
        <ButtonComponent
          title={isLoading ? <ActivityIndicator /> : 'Register'}
          onPress={() => {
            onSubmit();
          }}
          testID="button-Register"
          disable={isLoading}
        />
      </View>
      <Text style={styles.textDescription}>Already have an account?</Text>
      <LinkComponent
        title="Login"
        size={16}
        align="center"
        onPress={() => navigation.navigate('LoginScreen')}
        testID="button-Login"
      />
    </KeyboardAvoidingView>
  );
}

export default RegisterScreen;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.primary,
    paddingHorizontal: 13,
    paddingVertical: 13,
  },
  titleWrapper: {
    alignItems: 'center',
    marginTop: 90,
  },
  title: {
    fontFamily: fonts.primary[800],
    fontSize: 25,
    color: colors.text.primary,
  },
  text: {
    color: colors.text.secondary,
    fontFamily: fonts.primary[600],
  },
  inputWrapper: {
    marginTop: 24,
  },
  buttonWrapper: {
    marginTop: 29,
  },
  textDescription: {
    marginTop: 10,
    color: colors.text.secondary,
    fontFamily: fonts.primary[600],
    textAlign: 'center',
  },

});
