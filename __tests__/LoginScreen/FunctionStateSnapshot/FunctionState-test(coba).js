// import {
//   shallow, configure,
// } from 'enzyme';
// import React from 'react';
// import renderer from 'react-test-renderer';
// import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock';
// import Adapter from 'enzyme-adapter-react-16';
// import { Provider } from 'react-redux';
// import configureStore from 'redux-mock-store';
// import LoginScreen from '../../../src/pages/LoginScreen';
// import { authReducers, dataReducers } from '../../../src/redux';

// jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');
// jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);

// jest.mock('react-native-share', () => ({
// }));

// jest.mock('react-redux', () => ({
//   useSelector: jest.fn(),
//   useDispatch: () => mockDispatch,
// }));

// configure({ adapter: new Adapter(), disableLifecycleMethods: true });

// describe('Login Screen test', () => {
//   const initialState = {
//     dataBooks: dataReducers,
//     Auth: authReducers,
//   };

//   const mockStore = configureStore();
//   const store = mockStore(initialState);
//   const loginWrapper = shallow(
//     <Provider store={store}>
//       <LoginScreen />
//     </Provider>,
//   );

//   jest.mock('@react-navigation/native', () => ({
//     useNavigation: (component) => component,
//   }));

//   it('should render correctly', () => {
//     renderer.create(<Provider store={store}><LoginScreen /></Provider>);
//   });

//   it('should renders `LoginScreen Screen` module correctly', () => {
//     expect(loginWrapper).toMatchSnapshot();
//   });

//   describe('Check component', () => {
//     it('should have an email field', () => {
//       expect(loginWrapper.find('Input[testID="input-email"]').exists());
//     });
//     it('should have a password field', () => {
//       expect(loginWrapper.find('Input[testID="input-password"]').exists());
//     });
//   });

//   describe('Check function', () => {
//     it('should login', () => {
//       const onSubmitMock = jest.fn();

// loginWrapper.find('Input[testID="input-email"]')
//   .simulate('changeText', { target: { value: 'myUser' } });
// loginWrapper.find('Input[testID="input-password"]')
//   .simulate('changeText', { target: { value: 'myPassword' } });
//       loginWrapper.find('[testID="login-button"]').simulate('press');

//       expect(onSubmitMock).toHaveBeenCalled();
//     });
//   });
// });
