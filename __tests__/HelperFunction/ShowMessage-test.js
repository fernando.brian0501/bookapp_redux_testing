import { showMessage } from 'react-native-flash-message';
import { colors, showError, showSuccess } from '../../src/utils';

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');

describe('showError', () => {
  it('should show error message', () => {
    const message = 'Error message';
    expect(showError(message)).toBe(showMessage({
      message: 'Error message',
      type: 'default',
      backgroundColor: colors.background.secondary,
      color: colors.white,
    }));
  });
});

describe('showSuccess', () => {
  it('should show success message', () => {
    const message = 'Success message';
    expect(showSuccess(message)).toBe(showMessage({
      message: 'Success message',
      type: 'default',
      backgroundColor: colors.background.secondary,
      color: colors.white,
    }));
  });
});
