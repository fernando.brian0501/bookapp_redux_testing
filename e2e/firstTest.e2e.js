import { faker } from '@faker-js/faker';

describe('Flow Auth', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  const goToLogin = async () => {
    await expect(element(by.id('SplashScreen'))).toBeVisible();
    await waitFor(element(by.id('LoginScreen'))).toBeVisible().withTimeout(4000);
    await expect(element(by.id('LoginScreen'))).toBeVisible();
  };

  const login = async () => {
    await element(by.id('input-email')).typeText('cobacoba@gmail.com');
    await element(by.id('input-password')).typeText('cobacoba12');
    await element(by.id('input-password')).tapReturnKey();
    await element(by.id('btn-login')).tap();
    await waitFor(element(by.id('button-Logout'))).toBeVisible().withTimeout(6000);
    await element(by.id('button-Logout')).tap();
    await element(by.text('Ya')).tap();
  };

  const register = async () => {
    await element(by.id('input-FullName')).typeText('coba');
    await element(by.id('input-Email')).typeText(faker.internet.email());
    await element(by.id('input-Password')).typeText('Cobacoba12');
    await element(by.id('input-Password')).tapReturnKey();
    await element(by.id('button-Register')).tap();
  };

  const successRegister = async () => {
    await element(by.id('button-BackToLogin')).tap();
  };

  const flowAuth = async () => {
    await goToLogin();
    await element(by.id('btn-register')).tap();
    await register();
    await successRegister();
    await login();
  };
  describe('FlowAuth', () => {
    it('should login', async () => {
      await goToLogin();
      await login();
    });
    it('should register', async () => {
      await goToLogin();
      await element(by.id('btn-register')).tap();
      await register();
      await successRegister();
    });
    it('should flow auth', async () => {
      await flowAuth();
    });
  });
});
