import ReduxThunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import axios from 'axios';
import {
  refresh, saveBookPopular, saveBookRecommended, setLoading,
  getDataBooksPopular, getDataBooksRecommended,
} from '../../../src/redux/action';
import { Data, dataResults } from './DummyDataBook';
import { AverageRatingSort } from '../../../src/utils/helperFunction';

jest.mock('axios');

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');

describe('Action Test', () => {
  it('Refresh Action Test', () => {
    expect(refresh(true)).toEqual({
      type: '@SET_REFRESHING',
      payload: true,
    });
  });

  it('Save Book Popular Action Test', () => {
    expect(saveBookPopular([])).toEqual({
      type: '@GET_BOOKS_POPULAR',
      payload: [],
    });
  });

  it('Save Book Recommended Action Test', () => {
    expect(saveBookRecommended([])).toEqual({
      type: '@GET_BOOKS_RECOMMENDED',
      payload: [],
    });
  });

  it('Set Loading Action Test', () => {
    expect(setLoading(true)).toEqual({
      type: '@SET_LOADING',
      payload: true,
    });
  });
});

describe('Get Data Action Test', () => {
  const middlewares = [ReduxThunk];
  const mockStore = configureMockStore(middlewares);
  const store = mockStore({});

  // Get recommended
  test('should get recommended book ', async () => {
    axios.post.mockImplementation(() => Promise.resolve({
      status: 200,
      data: Data,
    }));

    const token = 'testToken';
    const limit = 6;
    const dataSort = dataResults.sort(AverageRatingSort);
    await store.dispatch(getDataBooksRecommended(token, limit)).then(() => {
      expect(store.getActions()[0]).toEqual(
        setLoading(true),
        saveBookRecommended(dataSort),
      );
    });
  });

  // Get popular
  test('should Get Popular book', async () => {
    axios.post.mockImplementation(() => Promise.resolve({
      status: 200,
      data: Data,
    }));
    const token = 'testToken';
    let dataSort;

    await store.dispatch(getDataBooksPopular(token)).then(() => {
      expect(store.getActions()[0]).toEqual(
        setLoading(true),
        dataSort = dataResults.sort(AverageRatingSort),
        saveBookPopular(dataSort),

      );
    });
  });
});
