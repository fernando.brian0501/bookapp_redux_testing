export function AverageRatingSort(a, b) {
  return parseFloat(b.average_rating) - parseFloat(a.average_rating);
}
