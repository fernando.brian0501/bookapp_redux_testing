import {
  StyleSheet, Text, View, ActivityIndicator, StatusBar,
} from 'react-native';
import React from 'react';
import { colors, fonts } from '../../utils';

function Loading() {
  return (
    <View style={styles.wrapper}>
      <StatusBar barStyle="dark-content" backgroundColor={colors.background.primary} />
      <ActivityIndicator size="large" color={colors.background.secondary} />
      <Text style={styles.text}>Loading ...</Text>
    </View>
  );
}

export default Loading;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    position: 'absolute',
    alignItems: 'center',
    backgroundColor: colors.background.primary,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
  text: {
    fontFamily: fonts.primary[600],
    fontSize: 18,
    color: colors.text.primary,
    marginTop: 12,
  },
});
