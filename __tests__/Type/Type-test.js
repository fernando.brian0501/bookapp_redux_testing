import {
  GET_BOOKS_ID,
  GET_BOOKS_POPULAR,
  GET_BOOKS_RECOMMENDED,
  LOGIN,
  LOGIN_LOADING, LOGOUT, REGISTER, REGISTER_LOADING, SET_LOADING, SET_REFRESHING,
} from '../../src/redux/types';

test('Type Test', () => {
  expect(GET_BOOKS_ID).toBe('@GET_BOOKS_ID');
  expect(GET_BOOKS_POPULAR).toBe('@GET_BOOKS_POPULAR');
  expect(GET_BOOKS_RECOMMENDED).toBe('@GET_BOOKS_RECOMMENDED');
  expect(LOGIN).toBe('@LOGIN');
  expect(LOGIN_LOADING).toBe('@LOGIN_LOADING');
  expect(LOGOUT).toBe('@LOGOUT');
  expect(REGISTER).toBe('@REGISTER');
  expect(REGISTER_LOADING).toBe('@REGISTER_LOADING');
  expect(SET_LOADING).toBe('@SET_LOADING');
  expect(SET_REFRESHING).toBe('@SET_REFRESHING');
});
