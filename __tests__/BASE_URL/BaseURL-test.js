import { API_BASE_URL } from '../../src/config';

describe('API_BASE_URL', () => {
  it('should return http://code.aldipee.com/api/v1', () => {
    expect(API_BASE_URL).toBe('http://code.aldipee.com/api/v1');
  });
});
