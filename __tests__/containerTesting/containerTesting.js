import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { Persistore, Store } from '../../src/redux';

export default function ContainerTesting(screen) {
  return (
    <Provider store={Store}>
      <PersistGate persistor={Persistore}>{screen}</PersistGate>
    </Provider>
  );
}
