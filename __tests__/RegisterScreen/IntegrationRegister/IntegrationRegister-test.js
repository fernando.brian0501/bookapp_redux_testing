import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { REGISTER_USER } from '../../../src/config';

const registerApi = async (em, password, name) => {
  try {
    return await axios.post(REGISTER_USER, { em, password, name });
  } catch (e) {
    return [];
  }
};

describe('REGISTER', () => {
  let mock;

  beforeAll(() => {
    mock = new MockAdapter(axios);
  });

  afterEach(() => {
    mock.reset();
  });
  describe('registerApi', () => {
    it('should return an object', async () => {
      const response = {
        success: true,
        message: 'Register succeed! Please try to login with email sabrinabinr@pmail.com',
        data: {
          role: 'user',
          isEmailVerified: true,
          email: 'sabrinabinr@pmail.com',
          name: 'Sabrina',
          id: '625d761262fc712fb9a2541b',
        },
      };
      mock.onPost(REGISTER_USER).reply(201, response);
      const result = await registerApi('sabrinabinr@pmail.com', 'Binar2022', 'Sabrina');
      expect(result.data).toEqual(response);
    });
    it('should return an empty object', async () => {
      mock.onPost(REGISTER_USER).reply(400, []);
      const result = await registerApi('sabrinabinr@pmail.com', 'Binar2022', 'Sabrina');
      expect(result).toEqual([]);
    });
  });
});
